<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/custom/wishlist.css">
<section class="single-banner">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="single-content"><h2>Wishlist</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                        <li class="breadcrumb-item"><a href="product-list-1.html">Product-list-1</a></li>
                        <li class="breadcrumb-item"><a href="product-details-1.html">Product-details-1</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Wishlist</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="wish-part">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="wish-list">
                    <table class="table-list">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Product</th>
                            <th scope="col">Name</th>
                            <th scope="col">Price</th>
                            <th scope="col">Status</th>
                            <th scope="col">Sopping</th>
                            <th scope="col">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="table-number"><h5>01</h5></td>
                            <td class="table-product"><img src="<?php echo get_template_directory_uri(); ?>/images/product/01.jpg" alt="product"></td>
                            <td class="table-name"><h5>Heriloom Quinoa</h5></td>
                            <td class="table-price"><h5>$18.00</h5></td>
                            <td class="table-status"><h5>In Stock</h5></td>
                            <td class="table-shop">
                                <button class="btn btn-inline">Add to Cart</button>
                            </td>
                            <td class="table-action"><a href="#"><i class="fas fa-trash-alt"></i></a></td>
                        </tr>
                        <tr>
                            <td class="table-number"><h5>02</h5></td>
                            <td class="table-product"><img src="<?php echo get_template_directory_uri(); ?>/images/product/02.jpg" alt="product"></td>
                            <td class="table-name"><h5>Red Bulgur</h5></td>
                            <td class="table-price"><h5>$23.00</h5></td>
                            <td class="table-status"><h5>In Stock</h5></td>
                            <td class="table-shop">
                                <button class="btn btn-inline">Add to Cart</button>
                            </td>
                            <td class="table-action"><a href="#"><i class="fas fa-trash-alt"></i></a></td>
                        </tr>
                        <tr>
                            <td class="table-number"><h5>03</h5></td>
                            <td class="table-product"><img src="<?php echo get_template_directory_uri(); ?>/images/product/03.jpg" alt="product"></td>
                            <td class="table-name"><h5>Silken Tofu</h5></td>
                            <td class="table-price"><h5>$35.00</h5></td>
                            <td class="table-status"><h5>In Stock</h5></td>
                            <td class="table-shop">
                                <button class="btn btn-inline">Add to Cart</button>
                            </td>
                            <td class="table-action"><a href="#"><i class="fas fa-trash-alt"></i></a></td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="wish-btn"><a href="shop-leftbar.html" class="btn btn-inline"><i class="fas fa-undo-alt"></i><span>Continue Shopping</span></a><a
                        href="checkout.html" class="btn btn-inline"><i class="fas fa-check"></i><span>Proceed to Checkout</span></a>
                </div>
            </div>
        </div>
    </div>
</section>