<h1>we need a slider here</h1>
<section class="section trend-part">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-heading"><h2 class="title">Trending products</h2><a href="product-list-1.html"
                                                                                        class="btn btn-outline"><i
                            class="fas fa-eye"></i>show more</a></div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <div class="product-card">
                    <div class="product-img"><img
                            src="<?php echo get_template_directory_uri(); ?>/images/product/01.jpg"
                            alt="product">
                        <ul class="product-widget">
                            <li>
                                <button><i class="fas fa-eye"></i></button>
                            </li>
                            <li>
                                <button><i class="fas fa-heart"></i></button>
                            </li>
                            <li>
                                <button><i class="fas fa-exchange-alt"></i></button>
                            </li>
                        </ul>
                    </div>
                    <div class="product-content">
                        <div class="product-name"><h6><a href="#">Product Name</a></h6></div>
                        <div class="product-price">
                            <h6>
                                <del>$80</del>
                                $150
                            </h6>
                            <div class="product-rating"><i class="fas fa-star"></i><span>4.5/2</span></div>
                        </div>
                        <div class="product-btn"><a href="#"><i
                                    class="fas fa-shopping-basket"></i><span>Add to Cart</span></a></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="product-card">
                    <div class="product-img"><img
                            src="<?php echo get_template_directory_uri(); ?>/images/product/01.jpg"
                            alt="product">
                        <ul class="product-widget">
                            <li>
                                <button><i class="fas fa-eye"></i></button>
                            </li>
                            <li>
                                <button><i class="fas fa-heart"></i></button>
                            </li>
                            <li>
                                <button><i class="fas fa-exchange-alt"></i></button>
                            </li>
                        </ul>
                    </div>
                    <div class="product-content">
                        <div class="product-name"><h6><a href="#">Product Name</a></h6></div>
                        <div class="product-price">
                            <h6>
                                <del>$80</del>
                                $150
                            </h6>
                            <div class="product-rating"><i class="fas fa-star"></i><span>4.5/2</span></div>
                        </div>
                        <div class="product-btn"><a href="#"><i
                                    class="fas fa-shopping-basket"></i><span>Add to Cart</span></a></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="product-card">
                    <div class="product-img"><img
                            src="<?php echo get_template_directory_uri(); ?>/images/product/01.jpg"
                            alt="product">
                        <ul class="product-widget">
                            <li>
                                <button><i class="fas fa-eye"></i></button>
                            </li>
                            <li>
                                <button><i class="fas fa-heart"></i></button>
                            </li>
                            <li>
                                <button><i class="fas fa-exchange-alt"></i></button>
                            </li>
                        </ul>
                    </div>
                    <div class="product-content">
                        <div class="product-name"><h6><a href="#">Product Name</a></h6></div>
                        <div class="product-price">
                            <h6>
                                <del>$80</del>
                                $150
                            </h6>
                            <div class="product-rating"><i class="fas fa-star"></i><span>4.5/2</span></div>
                        </div>
                        <div class="product-btn"><a href="#"><i
                                    class="fas fa-shopping-basket"></i><span>Add to Cart</span></a></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="product-card">
                    <div class="product-img"><img
                            src="<?php echo get_template_directory_uri(); ?>/images/product/01.jpg"
                            alt="product">
                        <ul class="product-widget">
                            <li>
                                <button><i class="fas fa-eye"></i></button>
                            </li>
                            <li>
                                <button><i class="fas fa-heart"></i></button>
                            </li>
                            <li>
                                <button><i class="fas fa-exchange-alt"></i></button>
                            </li>
                        </ul>
                    </div>
                    <div class="product-content">
                        <div class="product-name"><h6><a href="#">Product Name</a></h6></div>
                        <div class="product-price">
                            <h6>
                                <del>$80</del>
                                $150
                            </h6>
                            <div class="product-rating"><i class="fas fa-star"></i><span>4.5/2</span></div>
                        </div>
                        <div class="product-btn"><a href="#"><i
                                    class="fas fa-shopping-basket"></i><span>Add to Cart</span></a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section trend-part">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-heading"><h2 class="title">Best selling products</h2><a href="product-list-1.html"
                                                                                            class="btn btn-outline"><i
                            class="fas fa-eye"></i>show more</a></div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <div class="product-card">
                    <div class="product-img"><img
                            src="<?php echo get_template_directory_uri(); ?>/images/product/01.jpg"
                            alt="product">
                        <ul class="product-widget">
                            <li>
                                <button><i class="fas fa-eye"></i></button>
                            </li>
                            <li>
                                <button><i class="fas fa-heart"></i></button>
                            </li>
                            <li>
                                <button><i class="fas fa-exchange-alt"></i></button>
                            </li>
                        </ul>
                    </div>
                    <div class="product-content">
                        <div class="product-name"><h6><a href="#">Product Name</a></h6></div>
                        <div class="product-price">
                            <h6>
                                <del>$80</del>
                                $150
                            </h6>
                            <div class="product-rating"><i class="fas fa-star"></i><span>4.5/2</span></div>
                        </div>
                        <div class="product-btn"><a href="#"><i
                                    class="fas fa-shopping-basket"></i><span>Add to Cart</span></a></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="product-card">
                    <div class="product-img"><img
                            src="<?php echo get_template_directory_uri(); ?>/images/product/01.jpg"
                            alt="product">
                        <ul class="product-widget">
                            <li>
                                <button><i class="fas fa-eye"></i></button>
                            </li>
                            <li>
                                <button><i class="fas fa-heart"></i></button>
                            </li>
                            <li>
                                <button><i class="fas fa-exchange-alt"></i></button>
                            </li>
                        </ul>
                    </div>
                    <div class="product-content">
                        <div class="product-name"><h6><a href="#">Product Name</a></h6></div>
                        <div class="product-price">
                            <h6>
                                <del>$80</del>
                                $150
                            </h6>
                            <div class="product-rating"><i class="fas fa-star"></i><span>4.5/2</span></div>
                        </div>
                        <div class="product-btn"><a href="#"><i
                                    class="fas fa-shopping-basket"></i><span>Add to Cart</span></a></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="product-card">
                    <div class="product-img"><img
                            src="<?php echo get_template_directory_uri(); ?>/images/product/01.jpg"
                            alt="product">
                        <ul class="product-widget">
                            <li>
                                <button><i class="fas fa-eye"></i></button>
                            </li>
                            <li>
                                <button><i class="fas fa-heart"></i></button>
                            </li>
                            <li>
                                <button><i class="fas fa-exchange-alt"></i></button>
                            </li>
                        </ul>
                    </div>
                    <div class="product-content">
                        <div class="product-name"><h6><a href="#">Product Name</a></h6></div>
                        <div class="product-price">
                            <h6>
                                <del>$80</del>
                                $150
                            </h6>
                            <div class="product-rating"><i class="fas fa-star"></i><span>4.5/2</span></div>
                        </div>
                        <div class="product-btn"><a href="#"><i
                                    class="fas fa-shopping-basket"></i><span>Add to Cart</span></a></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="product-card">
                    <div class="product-img"><img
                            src="<?php echo get_template_directory_uri(); ?>/images/product/01.jpg"
                            alt="product">
                        <ul class="product-widget">
                            <li>
                                <button><i class="fas fa-eye"></i></button>
                            </li>
                            <li>
                                <button><i class="fas fa-heart"></i></button>
                            </li>
                            <li>
                                <button><i class="fas fa-exchange-alt"></i></button>
                            </li>
                        </ul>
                    </div>
                    <div class="product-content">
                        <div class="product-name"><h6><a href="#">Product Name</a></h6></div>
                        <div class="product-price">
                            <h6>
                                <del>$80</del>
                                $150
                            </h6>
                            <div class="product-rating"><i class="fas fa-star"></i><span>4.5/2</span></div>
                        </div>
                        <div class="product-btn"><a href="#"><i
                                    class="fas fa-shopping-basket"></i><span>Add to Cart</span></a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section trend-part">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-heading"><h2 class="title">New products</h2><a href="product-list-1.html"
                                                                                   class="btn btn-outline"><i
                            class="fas fa-eye"></i>show more</a></div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <div class="product-card">
                    <div class="product-img"><img
                            src="<?php echo get_template_directory_uri(); ?>/images/product/01.jpg"
                            alt="product">
                        <ul class="product-widget">
                            <li>
                                <button><i class="fas fa-eye"></i></button>
                            </li>
                            <li>
                                <button><i class="fas fa-heart"></i></button>
                            </li>
                            <li>
                                <button><i class="fas fa-exchange-alt"></i></button>
                            </li>
                        </ul>
                    </div>
                    <div class="product-content">
                        <div class="product-name"><h6><a href="#">Product Name</a></h6></div>
                        <div class="product-price">
                            <h6>
                                <del>$80</del>
                                $150
                            </h6>
                            <div class="product-rating"><i class="fas fa-star"></i><span>4.5/2</span></div>
                        </div>
                        <div class="product-btn"><a href="#"><i
                                    class="fas fa-shopping-basket"></i><span>Add to Cart</span></a></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="product-card">
                    <div class="product-img"><img
                            src="<?php echo get_template_directory_uri(); ?>/images/product/01.jpg"
                            alt="product">
                        <ul class="product-widget">
                            <li>
                                <button><i class="fas fa-eye"></i></button>
                            </li>
                            <li>
                                <button><i class="fas fa-heart"></i></button>
                            </li>
                            <li>
                                <button><i class="fas fa-exchange-alt"></i></button>
                            </li>
                        </ul>
                    </div>
                    <div class="product-content">
                        <div class="product-name"><h6><a href="#">Product Name</a></h6></div>
                        <div class="product-price">
                            <h6>
                                <del>$80</del>
                                $150
                            </h6>
                            <div class="product-rating"><i class="fas fa-star"></i><span>4.5/2</span></div>
                        </div>
                        <div class="product-btn"><a href="#"><i
                                    class="fas fa-shopping-basket"></i><span>Add to Cart</span></a></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="product-card">
                    <div class="product-img"><img
                            src="<?php echo get_template_directory_uri(); ?>/images/product/01.jpg"
                            alt="product">
                        <ul class="product-widget">
                            <li>
                                <button><i class="fas fa-eye"></i></button>
                            </li>
                            <li>
                                <button><i class="fas fa-heart"></i></button>
                            </li>
                            <li>
                                <button><i class="fas fa-exchange-alt"></i></button>
                            </li>
                        </ul>
                    </div>
                    <div class="product-content">
                        <div class="product-name"><h6><a href="#">Product Name</a></h6></div>
                        <div class="product-price">
                            <h6>
                                <del>$80</del>
                                $150
                            </h6>
                            <div class="product-rating"><i class="fas fa-star"></i><span>4.5/2</span></div>
                        </div>
                        <div class="product-btn"><a href="#"><i
                                    class="fas fa-shopping-basket"></i><span>Add to Cart</span></a></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="product-card">
                    <div class="product-img"><img
                            src="<?php echo get_template_directory_uri(); ?>/images/product/01.jpg"
                            alt="product">
                        <ul class="product-widget">
                            <li>
                                <button><i class="fas fa-eye"></i></button>
                            </li>
                            <li>
                                <button><i class="fas fa-heart"></i></button>
                            </li>
                            <li>
                                <button><i class="fas fa-exchange-alt"></i></button>
                            </li>
                        </ul>
                    </div>
                    <div class="product-content">
                        <div class="product-name"><h6><a href="#">Product Name</a></h6></div>
                        <div class="product-price">
                            <h6>
                                <del>$80</del>
                                $150
                            </h6>
                            <div class="product-rating"><i class="fas fa-star"></i><span>4.5/2</span></div>
                        </div>
                        <div class="product-btn"><a href="#"><i
                                    class="fas fa-shopping-basket"></i><span>Add to Cart</span></a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="feature-part" style="margin-top: 15px">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-lg-4">
                <div class="feature-card"><i class="flaticon-delivery-truck"></i>
                    <h3>Free Delivery</h3>
                    <p>Tempora odio reiciendis incidunt tenetur deserunt dolores autem beatae</p></div>
            </div>
            <div class="col-md-4 col-lg-4">
                <div class="feature-card"><i class="flaticon-save-money"></i>
                    <h3>Instant Return</h3>
                    <p>Tempora odio reiciendis incidunt tenetur deserunt dolores autem beatae</p></div>
            </div>
            <div class="col-md-4 col-lg-4">
                <div class="feature-card"><i class="flaticon-customer-service"></i>
                    <h3>Quick Support</h3>
                    <p>Tempora odio reiciendis incidunt tenetur deserunt dolores autem beatae</p></div>
            </div>
        </div>
    </div>
</section>
<section class="news-part">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-lg-6">
                <div class="news-content"><h2>Subscribe for Latest Offers</h2></div>
            </div>
            <div class="col-md-7 col-lg-6">
                <form class="search-form news-form"><input type="text" placeholder="Enter Email Address">
                    <button class="btn btn-inline"><i class="fas fa-envelope"></i><span>Subscribe</span></button>
                </form>
            </div>
        </div>
    </div>
</section>
