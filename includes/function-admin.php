<?php
/*
    @package clover theme
        ==================================
                ADMIN PAGE
        ==================================
 */

function clover_add_admin_page()
{
    add_menu_page('Clover Theme Options', 'Clover', 'manage_options', 'technography-clover', 'clover_theme_create_page',
        '', 110);
}

add_action('admin_menu', 'clover_add_admin_page');

function clover_theme_create_page()
{
    require_once ( get_template_directory() . '/includes/temp-admin.php');
}