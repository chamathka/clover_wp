var SHOP_ID = '88M9MWYTSCH51';
var Authorization_Headers = {
    Accept: 'application/json',
    Authorization: 'Bearer 2e9450dc-74e8-9b5f-6f01-88427b6c4d02'
}

function api_setup_shop_id(data) {
    SHOP_ID = data.value
    console.log('SHOP ID SETUP : ' + SHOP_ID)
}

function api_setup_shop_token(data) {
    Authorization_Headers = {
        Accept: 'application/json',
        Authorization: 'Bearer ' + data.value
    }
    console.log('SHOP TOKEN SETUP : ' + Authorization_Headers.Authorization)
}

function fetch_categories() {
    const options = {method: 'GET', headers: Authorization_Headers};
    return fetch('https://try.readme.io/https://sandbox.dev.clover.com/v3/merchants/' + SHOP_ID + '/categories', options)
        .then(response => response.json())
        .catch(err => console.error(err));
}
function fetch_items() {
    const options = {method: 'GET', headers: Authorization_Headers};
    return fetch('https://try.readme.io/https://sandbox.dev.clover.com/v3/merchants/' + SHOP_ID + '/items', options)
        .then(response => response.json())
        .catch(err => console.error(err));
}